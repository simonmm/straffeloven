# Straffeloven 2005 - se [lovdata](https://lovdata.no/dokument/NL/lov/2005-05-20-28/)
* Loven kan anvendes f.eks ved:
    * Innbrudd i datasystemer (§204)
        * Bot eller opp til 2 år med fengsel dersom adgangen
          involverer brudd av beskyttelse eller på annen måte er "uberettiget"
        * Også relevant til dette er nok Uberettiget befatning med tilgangsdata, 
          dataprogram mv. (§201) som retter seg mot de som med forsett
          om å begå kriminalitet fremstiller passord eller dataprogram
          [...] som er særlig egnet til å begå kriminalitet
        * Krenkelse av retten til privat informasjon (§205) som går
          på de som for eksempel bryter seg inn og leser privat informasjon
    * Krenkelse av forretningshemmelighet (§207, §208)
        * Bot eller opp til 2 år fengsel dersom den ansatte
          "gjør bruk av" hemmeligheten eller "gjør den kjent for en annen"
    * Logiske bomber eller datavirus som medfører skade (§351 og §352)
        * Forstår det slik at dette vil gå under loven for skadeverk (§351)
          og grovt skadeverk (§352) avhengig av alvorsgraden
        * Er også tenkelig dette kan gå under Fare for driftshindring (§206)
          som gir opp til 2 år fengsel for å forstyrre et datasystem
        * Det er ikke umulig at enkelte dataangrep vil kunne gå under
          terrorlover heller (§131, §132 m. fl.). For eksempel
          angrep på kritisk infrastruktur med enorme konsekvenser.
    * Forfalskning, også ved hjelp av datamaskiner (§202, §370, §371)
        * §202 går på identitetsforfalskning mens §370-371 går på
          bedrageri som også kan være datakriminalitet
    * Konklusjon: Det er vanskelig å gjengi de nye lovene for data
    sikkert fordi Straffeloven er en såpass stor tekst og fordi veldig mye av jus
    handler om skikk og bruk. Men ut ifra dette forsøket virker det som
    om den nye straffeloven er mer detaljert og omfattende på
    moderne kriminalitet som for eksempel det som angår det digitale.




